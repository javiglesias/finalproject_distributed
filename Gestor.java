package code;
//http://localhost:8080/Bully_algorithm/main/
import java.net.URI;
import java.util.Scanner;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;

public class Gestor
{
	public static int n_procesos = 6;
	public static int n_maquinas = 3;
	public static String []ips = new String[n_maquinas];
	public static WebTarget []target = new WebTarget[n_procesos];
	public static String concatenar = "";
	public static Client cliente = ClientBuilder.newClient();
	public static URI[] url = new URI[n_procesos];
	public static String apagados = "";
	public static String encendidos = "";
	public static Scanner sc;

	public static void main(String[] args) 
	{
		ips[0] = "localhost:8080";
		for (int i = 1; i < ips.length; i++) 
		{
			sc = new Scanner(System.in);
			System.out.print("Maquina "+i+" (ip:port): ");
			ips[i] = sc.nextLine();
			concatenar += ips[i]+"~";
		}
		//CONECTAR
		for (int i = 0; i < ips.length; i++)
		{
		if(target[i]= cliente.target("http://"+ips[i]+"/Bully_algorithm/main/") == null)
			System.out.println("Error al conectarse al equipo "+ips[i]);
		if(target[i].path("concatenar").queryParam("concatenar", concatenar).
				request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class) == null)
			System.err.print("Error al mandar la concatenacion.");
		System.out.println(target[i].path("arrancar").
				request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class));
		}
		menu();
	}
	public static void menu()
	{
		int choose = 0,n = 0,m = 0;
		while (choose <= 0)
		{
			System.out.println("->MENU");
			System.out.println("1 - Apagar un servidor.");
			System.out.println("2 - Relanzar un servidor.");
			System.out.println("3 - Consultar el estado de un servidor.");
			System.out.println("4 - Terminar.");
			System.out.print("num: ");
			choose = sc.nextInt();
			switch(choose)
			{
			case 1:
				//TURN_OFF_SERVER
				n = 0,m = 0;
				System.out.println("El estado de los servidores es: " );
				State_server();
				System.out.println("Elija la maquina:");
				m = sc.nextInt();
				System.out.print("Que servidor quieres apagar?: ");
				n = sc.nextInt();
				System.out.println(target[m].path("end").queryParam("nproceso", n).
						request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class));
				State_server();
				choose = 0;
				break;
			case 2:
				//START_SERVERS
				n = 0,m = 0;
				System.out.println("El estado de los servidores es: " );
				State_server();
				System.out.println("Elija la maquina:");
				m = sc.nextInt();
				System.out.print("Que servidor quieres relanzar?: ");
				n = sc.nextInt();
				System.out.println(target[m].path("rearrancar").queryParam("nproceso", n).
						request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class));
				State_server();	
				choose = 0;
				break;
			case 3:
				State_server();
				choose = 0;
				break;
			case 4:
				System.out.println("ADIOS!!!!!");
				System.exit(0);
			default:
				choose = 0;
				break;	
			}
		}
	}
	public static void State_server()
	{
		String str = "";
		apagados = "";
		encendidos = "";
		for (int i = 0; i < n_maquinas; i++) 
		{
			for (int j = 0; j < 2; j++) 
			{
				str = target[i].path("state").queryParam("nproceso", j).
						request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
				if (str.equals("false")) 
					apagados += "Maquina "+i+" ("+ips[i]+") "+" Proceso "+j+":"+str+"\n";
				else
					encendidos += "Maquina "+i+" ("+ips[i]+") "+" Proceso "+j+":"+str+"\n";
				System.out.println("Maquina "+i+" ("+ips[i]+") "+" Proceso "+j+":"+str);
			}
		}
		
	}
}