package code;
//http://localhost:8080/Bully_algorithm/main/
import java.net.URI;
import java.util.ArrayList;
import java.util.Scanner;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;

public class Gestor
{
	int n_procesos = 6;
	//int n_maquinas = 3;
	//////////////////////////////////////////////////////----------------------
	static ArrayList<String> ips = new ArrayList<String>();
	static ArrayList<WebTarget> target = new ArrayList<WebTarget>();
	//static ArrayList<URI> url = new ArrayList<URI>();
	static String []deconcatenado;
	public static String concatenar = "";
	public static Client cliente = ClientBuilder.newClient();
	public static String apagados = "";
	public static String encendidos = "";
	static Scanner sc;
	static int nprocesos_maq = 0;
	static int n_maquinas = 0;
	////////////////////////////////////////////-------------------------------
	public static void main(String[] args) 
	{
		String deconcatenados = "";
		/*Es la ip por defecto, siempre vamos a tener al menos nprocesos en el localhost*/
		ips.add(0,"localhost:8080");
		target.add(0,cliente.target("http://"+ips.get(0)+"/Bully_algorithm/main/"));
		deconcatenados = target.get(0).path("scanner").request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
		/*Deberia mandar desde scanner concatenadas todas las maquinas que tenemos para poder hacer lo de las ips*/
		System.out.println(deconcatenados);
		deconcatenado = deconcatenados.split("~");
		for (int i = 1; i<deconcatenado.length; ++i) 
		{
			if(i != deconcatenado.length-1)
				ips.add(i,deconcatenado[i]);	
			else
				n_maquinas = Integer.parseInt(deconcatenado[i]);
		}
		System.out.println("Cuantos procesos quiere por maquina?: ");
		sc = new Scanner(System.in);
		nprocesos_maq = sc.nextInt();
		for (int j = 0; j<ips.size(); ++j) 
		{
			target.add(j,cliente.target("http://"+ips.get(j)+"/Bully_algorithm/main/"));
		}
		System.out.println(n_maquinas);
		for (int i = 0; i < n_maquinas; ++i) 
		{
			if(target.get(i).path("arrancar").queryParam("nprocesos", nprocesos_maq).
				request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class) == null)
			{
				System.err.print("Error al arrancar procesos.");
			}
		}
		menu();
	}
	public static void menu()
	{
		int choose = 0, n = 0,m = 0;
		while (choose <= 0)
		{
			System.out.println("->MENU");
			System.out.println("1 - Apagar un servidor.");
			System.out.println("2 - Relanzar un servidor.");
			System.out.println("3 - Consultar el estado de un servidor.");
			System.out.println("4 - Terminar.");
			System.out.print("num: ");
			choose = sc.nextInt();
			switch(choose)
			{
			case 1:
				System.out.println("El estado de los servidores es: " );
				State_server();
				System.out.println("Elija la maquina:");
				m = sc.nextInt();
				System.out.print("Que servidor quieres apagar?: ");
				n = sc.nextInt();
				System.out.println(target.get(m).path("end").queryParam("nproceso", n).
						request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class));
				State_server();
				choose = 0;
				break;
			case 2:
				System.out.println("El estado de los servidores es: " );
				State_server();
				System.out.println("Elija la maquina:");
				m = sc.nextInt();
				System.out.print("Que servidor quieres relanzar?: ");
				n = sc.nextInt();
				System.out.println(target.get(m).path("rearrancar").queryParam("nproceso", n).
						request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class));
				State_server();	
				choose = 0;
				break;
			case 3:
				State_server();
				choose = 0;
				break;
			case 4:
				System.out.println("ADIOS!!!!!");
				System.exit(0);
			default:
				choose = 0;
				break;	
			}
		}
	}
	public static void State_server()
	{
		for (int j = 0; j < n_maquinas; j++)
		{
			for (int i = 0; i < nprocesos_maq; i++) 
			{
				System.out.println("Maquina: "+j+" Proceso: "+i+target.get(j).
					path("state").queryParam("nproceso", i).
					request(javax.ws.rs.core.MediaType.TEXT_PLAIN).
					get(String.class));	
			}
		}
	}
}