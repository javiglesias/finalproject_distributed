package Bully_algorithm;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;
import java.util.logging.Logger;

import org.glassfish.jersey.message.internal.TracingLogger.Level;

//http://localhost:8080/Bully_algorithm/main/
public class Proceso extends Thread implements Runnable
{
	Servicio supar = new Servicio();
	public boolean state;
	public int coord = 0;
	private int id_proceso = -1;
	
	public Proceso(int id)
	{
		this.set_id_proceso(id);
		System.out.println("arrancada: "+this.id_proceso);
	}
	public void run()
	{
		state = true;
		try {
			eleccion();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public boolean parada()
	{
		state = false;
		return state;
	}
	public int computar()
	{	
		Random rng = new Random();
		try {
			this.sleep((long) (500 + (rng.nextDouble()*(1000-500))));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return 0;
	}
	public void rearrancar() throws InterruptedException
	{
		state = true;
		comprobaciones();
		eleccion(); //cada vez que se arranque un proceso nuevo
	}
	public int comprobaciones()
	{
		Random rng = new Random();
		if(state)
		{
			System.out.println(this.getName()+this.get_id_proceso());
			try 
			{
				this.sleep((long) (500 + (rng.nextDouble()*(1000-500))));
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(this.id_proceso == coord)//En este caso sera -50 que es el return 
			//cuando el proceso esta con state=false.
			{
				this.computar();
			}
			return 0;//all ok
		}
		else 
			return 1;//error para saber que esta apagado el servidor
	}
	public boolean get_state()
	{
		return state;
	}
	public int get_id_proceso()
	{
		return id_proceso;
	}
	public void set_id_proceso(int id)
	{
		id_proceso = id;
	}
	public void eleccion() throws InterruptedException
	{
	   	//boolean eliguiendo = true;
	    //boolean fin = false;
	    //boolean msgOK = false;
	    //boolean mgsResponseCoord = false;
	    //boolean hay_coordinador = false;
	    
	   	if(state)
	   	{
		    if(this.id_proceso == supar.th.size()-1)
		    { 
		            coord=this.id_proceso; //El proceso actual se convierte en coordinador
		            //hay_coordinador=true; //Ya tenemos coordinador
		            //msgEnvioCoor();
		            System.out.println("Soy Coordinador: " + this.id_proceso);
		            //fin=true; //se acabo la eleccion
		    } 
		    else 
		    { // si no tenemos el mayor ID
		         //recorremos todos los procesos
		    	if(state)
		    	{
			        //supar.envioDeEleccion(); //Se envia la eleccion al restos de ids
			        synchronized(this)
			        {
			        	this.wait(1000L);
				        if(supar.enviar_mensaje(coord, "recibir_mensaje?nproceso="+coord) == -50)/*coordinador responde con -50*/
						{
					        /*el coordinador esta muerto, asi que tenemos que
					        enviar_eleccion a todos los procesos activos.*/
							supar.envioDeEleccion();
							System.out.println("Enviar eleccion a todos.");
						}
						else
						{
				            System.out.println("Todavia hay coordinador: "+coord);
				            //msgEnvioCoor();
				            //fin=true;
						}
					}
				}
			}
		}
    //eliguiendo=false; //se acaba de eleguir
	}
}