package Bully_algorithm;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;
import java.util.logging.Logger;

//http://localhost:8080/Bully_algorithm/main/
public class Proceso extends Thread implements Runnable
{
	Servicio supar = new Servicio();
	public boolean state;
	public int valor_coordinador = 0;
	//public static boolean hay_coordinador = false;
	private int id_proceso = -1;
	
	public Proceso(String name, int id)
	{
		this.setName(name);
		this.set_id_proceso(id);
	}
	public void run()
	{
		state = true;
	}
	public boolean parada()
	{
		state = false;
		return state;
	}
	public int computar()
	{
		if(!state)
			return -50;
		else
		{
			Random rng = new Random();
			try {
				this.sleep((long) (500 + (rng.nextDouble()*(1000-500))));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return 0;
		}
	}
	public void rearrancar()
	{
		state = true;
		//eleccion(); //cada vez que se arranque un proceso nuevo
	}
	public int comprobaciones()
	{
		Random rng = new Random();
		if(state)
		{
			System.out.println(this.getName()+this.get_id_proceso());
			try 
			{
				this.sleep((long) (500 + (rng.nextDouble()*(1000-500))));
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//valor = coordinador.computar();
			//if(valor < 0)//En este caso sera -50 que es el return 
			//cuando el proceso esta con state=false.
			//{
				//eleccion();
			//}
			return 0;//all ok
		}
		else 
			return 1;//error para saber que esta apagado el servidor
	}
	public boolean get_state()
	{
		return state;
	}
	public void set_id_proceso(int id)
	{
		id_proceso = id;
	}
	public void enviar_mensaje()
	{
		supar.avisar_proceso("localhost "+this.getName(),"\n"+"localhost "+this.get_id_proceso(),"\n"+"Ti amo");
	}
	public void recibir_mensaje(String proceso_origen,String proceso_destino,String mensaje)
	{
		supar.avisar_proceso("localhost "+this.getName(),"\n"+"localhost "+this.get_id_proceso(),"\n"+mensaje);
	}	
	public void eleccion(int id)
	{
	    boolean eliguiendo = true;
	    boolean fin = false;
	    boolean msgOK = false;
	    boolean mgsResponseCoord = false;
	    boolean hay_coordinador = false;
	    //=====================================================
	    //Comprobamos si tenemos el mayor ID
	    if(IdMayor())
	    { //Si soy el que tiene el ID mayor
	            coord=id; //El proceso actual se convierte en coordinador
	            hay_coordinador=true; //Ya tenemos coordinador
	            msgEnvioCoor();
	            fin=true; //se acabo la eleccion
	    } 
	    //=====================================================
	    else { // si no tenemos el mayor ID
	        //Mandamos eleccion al resto de procesos
	        for (int i=id+1;i<=num_procesos;i++ ) 
	        { //recorremos todos los procesos
	            System.out.println("El proceso"+ id +"manda la eleccion al proceso"+i);
	            envioDeEleccion(i); //Se envia la eleccion al restos de ids
	        }
	        //Esperamos 1s timeout Bloqueamos hilo
	            synchronized(this)
	            {
	                this.wait(1000); //ha de esperar 1s (no tiene espera ocupada)
	            }
	          // Si se recibe alguna respuesta...
			if(HeRecibidoRespuesta())
			{ //si recibo alguna respuesta
			    System.out.println("Al proceso" + id + "le han respondido");
			    try{
			        this.wait(1000); //ha de esperar 1s (no tiene espera ocupada)
			    } 
			    catch (InterruptedException ex) 
			    {
			    	Logger.getLogger(Proceso.class.getName()).log(Level.SEVERE, null, ex);
			    }
		        if(HeRecibidoRespuestaCoord())
		        { //si recibo respuesta del coordinador
	                System.out.println("Al proceso"+ id +"le han mandado msg coordinador");
	                fin=true;
		        }
			}
		}
    eliguiendo=false; //se acaba de eleguir
	}
	public boolean IdMayor(int id)
	{ // Nos devolvera un true en caso de ser nosotros el de mayor ID
	    for (int i=id+1;i<=num_procesos;i++ ) 
	    {

	        URL url;
	       // url = new URL(supar.getUrlBase(i) + "poia_pollo/" + i);
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	        conn.setRequestMethod("GET");

	//LEER IP DEL SERVICIO
	        /*if (conn.getResponseCode() == 200) {
	                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	                int activo = Integer.parseInt(br.readLine());
	                br.close();
	                if (activo > 0) {
	                    return false;
	                }
	            }*/
	    }
	    return false;
	}
	public void oK()
	{

	}
}
