package Bully_algorithm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/main")
public class Servicio extends Thread
{
	private static Proceso th0 = new Proceso("proceso0",0);
	private static Proceso th1 = new Proceso("proceso1",1);
	public static int []ids = new int[6];
	private static String[] bloque_ids = new String[6];

	public Servicio(){}	
	@GET
	@Path("/end")
	public  void end(@DefaultValue("-1")@QueryParam(value="nproceso")int nproceso)
	{
		if(nproceso == 0)
			th0.parada();
		if(nproceso == 1)
			th1.parada();
	}
	@GET
	@Path("/rearrancar")
	public void rearrancar(@DefaultValue("-1")@QueryParam(value="nproceso")int nproceso)
	{
		if(nproceso == 0)
			th0.rearrancar();
		else if(nproceso == 1)
			th1.rearrancar();	
	}
	@GET
	@Path("/state")
	public String state(@DefaultValue("-1")@QueryParam(value="nproceso")int nproceso)
	{
		/*
		 * Hay que devolver el estado de todos los server, tanto los UP como los DOWN
		llamada: URLConnection()
		respuesta: InputStream
		*/
		if(nproceso == 0)
			return th0.get_state()+"";
		
		if(nproceso == 1)
			return th1.get_state()+"";
		
		return "-1";//error desconocido
	}
	@GET
	@Path("/arrancar")
	public void arrancar()
	{
		if(ids[0] != 0)
		{
			ids[0] = 0;
			th0.set_id_proceso(0);
			th0.start();
			ids[1] = 1;
			th1.set_id_proceso(1);
			th1.start();
		}
		else
		{
			/*
			 *aqui hay que poner que les ponga un id superior 
			 */
		}
	}
	@GET
	@Path("/nyam")
	public String nyam
	{
		return "nyam";
	}
	@GET 
	@Path("/concatenar")
	public void concatenar(@DefaultValue("-1")@QueryParam(value="concatenar")String concatenado)
	{
		bloque_ids = concatenado.split("~");
	}
	/*GET
	@Path("/recibir")
	public int recibir_mensaje(@DefaultValue("-1")@QueryParam(value="mensaje")String mensaje)
	{
		System.out.println(mensaje);
		return 0;
	}*/
	public void avisar_proceso(String proceso_origen,String proceso_destino,String mensaje)
	{
		System.out.println(proceso_origen+":"+proceso_destino+":"+mensaje);	
		int id = 0;
		String line = "";
		bloque_ids[id] = "172.20.1.164:8080";
		URL conn;
		HttpURLConnection hUC;
		String env = proceso_origen+proceso_destino+mensaje;
		try {
			conn = new URL("http://"+bloque_ids[id]+"/Bully_algorithm/main/state?nproceso=1");
			hUC = (HttpURLConnection) conn.openConnection();
			hUC.disconnect();
			hUC.setRequestMethod("GET");
			if(hUC.getResponseCode() == 200)
			{
				BufferedReader in = new BufferedReader(
					new InputStreamReader(hUC.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				hUC.disconnect();
				System.out.println(response);
			}
			else
				System.out.println("hUC.getResponseCode()");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	@GET
	@Path("/loopBack")
	public void loopBack(@DefaultValue("-1")@QueryParam(value="nproceso")int nproceso)
	{
		if (nproceso == 0) 
			th0.enviar_mensaje();
		if (nproceso == 1)
			th1.enviar_mensaje(); 
	}
	@GET
	@Path("/eleccion")
	public void eleccion(@DefaultValue("-1")@QueryParam(value="nproceso")int nproceso)
	{
		if (nproceso == 0) 
			th0.eleccion();
		if (nproceso == 1)
			th1.eleccion();
	}
}