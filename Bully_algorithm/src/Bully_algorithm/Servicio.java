package Bully_algorithm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/main")
public class Servicio extends Thread
{
	private int MTimeout = 1000;	
	//private static Proceso[] th = new Proceso[2];
	//public static int []ids = new int[6];
	static ArrayList<String> ids = new ArrayList<String>();
	//almacenar los "ids" de los procesos en un List<Strings>, que coincidan 
	static ArrayList<Proceso> th = new ArrayList<Proceso>();
	//En este Array se meteran las ips:puerto de todos los procesos asociados a nuestro projecto
	static ArrayList<String> procesos = new ArrayList<String>();
	public Servicio(){}
	@GET
	@Path("/end")
	public  void end(@DefaultValue("-1")@QueryParam(value="nproceso")int nproceso)
	{
			th.get(nproceso).parada();
	}
	@GET
	@Path("/rearrancar")
	public void rearrancar(@DefaultValue("0")@QueryParam(value="nproceso")int nproceso) throws InterruptedException
	{
		System.out.println(th.size());
		th.get(nproceso).rearrancar();
	}
	@GET
	@Path("/state")
	public String state(@DefaultValue("-1")@QueryParam(value="nproceso")int nproceso)
	{
		//System.out.println(th.size());
		return th.get(nproceso).get_state()+"";
	}
	@GET
	@Path("/arrancar")
	public void arrancar(@DefaultValue("-1")@QueryParam(value="nprocesos")int nprocesos)
	{
//for from th0 to th.size() starting all the ths
		if(th.isEmpty())
		{
			for (int i = 0;i<nprocesos;++i)
			{
				th.add(i, new Proceso(i));
				th.get(i).start();	
			}
		}
		System.out.println(th.size());
	}
	@GET
	@Path("/nyam")
	public String meow()
	{
		return "meow";
	}
	@GET
	@Path("/scanner")
	public String scanNet()
	{
		URL conn;
		HttpURLConnection hUC;
		int timeout=1000;
		String inputLine = "";
		int maquinas = 0;
		String concatenar="";
	  // for (int j=1;j<3j++) 
	   //{	 
		   for (int i=30;i<50;i++)
		   {
		        try {
					conn = new URL("http://"+"192.168.1."+i+":8080"+"/Bully_algorithm/main/nyam");
					hUC = (HttpURLConnection) conn.openConnection();
					hUC.setConnectTimeout(MTimeout);
					hUC.setRequestMethod("GET");
			       if (hUC.getResponseCode() == 200)
			       {
			       		maquinas ++;
			        	System.out.println(conn+"Reachabl\n");
			        	BufferedReader in = new BufferedReader(
			        		new InputStreamReader(hUC.getInputStream()));
				       while ((inputLine = in.readLine()) != null) 
				       	{
							//System.out.println(inputLine);
							concatenar +=conn;
							concatenar += "~";
						}
			       	}
			       	else
			       		System.out.println(conn+"\t NOT\n");
			       hUC.disconnect();
		        }
		        catch (ProtocolException e) 
		        {
					// TODO Auto-generated catch block
					
				}
				catch (MalformedURLException e) 
				{
					// TODO Auto-generated catch block
				} 
				catch (IOException e) 
				{
					// TODO Auto-generated catch block
				}
		      // System.out.println(host+"NoReachabl\n"); 
		   	}
	  	//}
	   	System.out.println(concatenar+maquinas);
	   	return concatenar+maquinas;
	}
	/*@GET
	@Path("/avisar")
	public boolean calcular_timeout()
	{
		synchronized (th) 
		{	
			//timeout
			return true;
		}
		//return false;
	}*/
	public void envioDeEleccion()
	{
		for (int i = 0;i<ids.size();++i ) 
		{
			//enviarle el mensaje al proceso que se indique
			//y manejar el mensaje en el servicio para saber a que proceso va dirigido
			enviar_mensaje(i,"eleccion");
		}
	}
	//@GET
	//@Path("enviar_mensaje")
	public int enviar_mensaje(int nproc,String path)
	{
		/*Hay que idear alguna forma para decirle a cual de los procesos que hay
		en la maquina a la que vamos a mandar el mensaje queremos avisar,
		porque de momento solo sabemos cual es la ip de la maquina a la
		que queremos avisar pero no hemos ideado nada para decirle 
		a cual de los procesos es al que queremos que le transmita 
		nuestro mensaje.*/
		URL conn;
		HttpURLConnection hUC;
		int timeout=1000;
		String inputLine = "";
		int maquinas = 0;
		String concatenar="";
		try 
		{
			conn = new URL(ids.get(nproc));
			hUC = (HttpURLConnection) conn.openConnection();
			hUC.setConnectTimeout(MTimeout);
			hUC.setRequestMethod("GET");
	       if (hUC.getResponseCode() == 200)
	       {
	        	BufferedReader in = new BufferedReader(
	        		new InputStreamReader(hUC.getInputStream()));
	        	/*faltaria comprobar que el id del proceso al que se
	        	quiere avisar sea el del coordinador.*/
	        	if (in.readLine() == -50+"")
	    		{ 
	    			this.notifyAll();
	    			return 0;
	    		}
	    		else
	    			return -50;
	       	}
	       	else
	       		hUC.disconnect();
	    }
	    catch (ProtocolException e) 
	    {
			// TODO Auto-generated catch block
		}
		catch (MalformedURLException e) 
		{
			// TODO Auto-generated catch block
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
		}
		return 0;
	}
	@GET
	@Path("/recibir")
	public int recibir_mensaje(@DefaultValue("-1")@QueryParam(value="nproc")int nproc)
	{
		/*comprobamos a cual de los procesos que estan en esta maquina 
		se quiere llamar.*/	
		if(!th.get(nproc).state)
			return -50;
		else
			return 0;
	}

}//Fin de la cita

	/*@GET 
	@Path("/concatenar")
	public void concatenar(@DefaultValue("-1")@QueryParam(value="concatenar")String concatenado)
	{
		bloque_ids = concatenado.split("~");
	}
	GET
	@Path("/recibir")
	public int recibir_mensaje(@DefaultValue("-1")@QueryParam(value="mensaje")String mensaje)
	{
		System.out.println(mensaje);
		return 0;
	}
	public void avisar_coordinador(String proceso_origen,String proceso_destino,String mensaje)
	{
		System.out.println(proceso_origen+":"+proceso_destino+":"+mensaje);	
		int id = 0;
		String line = "";
		bloque_ids[id] = "172.20.1.164:8080";
		URL conn;
		HttpURLConnection hUC;
		String env = proceso_origen+proceso_destino+mensaje;
		try 
		{
			conn = new URL("http://"+bloque_ids[id]+"/Bully_algorithm/main/set_coord?concatena="+destino+"~"+origen);
			hUC = (HttpURLConnection) conn.openConnection();
			hUC.disconnect();
			hUC.setRequestMethod("GET");
			if(hUC.getResponseCode() == 200)
			{
				BufferedReader in = new BufferedReader(
					new InputStreamReader(hUC.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				hUC.disconnect();
				System.out.println(response);
			}
			else
				System.out.println("hUC.getResponseCode()");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
	}*/
		/*@GET
	@Path("/loopBack")
	public void loopBack(@DefaultValue("-1")@QueryParam(value="nproceso")int nproceso)
	{
		if (nproceso == 0) 
			th0.enviar_mensaje();
		if (nproceso == 1)
			th1.enviar_mensaje(); 
	}*/